﻿using SocialNetworkService.Entities;
using SocialNetworkService.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Facebook.Providers
{
    public class CommonProviders : ISocialNetworkProvider
    {
        public Task<IList<IAlbum>> GetAlbumsByUserId(string accessToken, string userId)
        {
            throw new NotImplementedException();
        }

        public Task<IAvatar> GetAvatarByUserId(string accessToken, string userId)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetEmailByUserId(string accessToken, string userId)
        {
            throw new NotImplementedException();
        }

        public Task<IList<IPhoto>> GetPhotosFromAlbum(string accessToken, string albumId)
        {
            throw new NotImplementedException();
        }

        public Task<IProfileInfo> GetProfileInfoByUserId(string accessToken, string userId)
        {
            throw new NotImplementedException();
        }
    }
}
