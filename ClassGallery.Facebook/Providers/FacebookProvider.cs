﻿using ClassGallery.Facebook.Entities;
using Facebook;
using Newtonsoft.Json;
using SocialNetworkService.Entities;
using SocialNetworkService.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Facebook.Providers
{
    public class FacebookProvider : ISocialNetworkProvider
    {
        protected readonly FacebookClient facebookClient;

        public FacebookProvider(FacebookClient facebookClient)
        {
            this.facebookClient = facebookClient;
        }

        private async Task<JsonObject> Get(string accessToken, string path, object parameters = null)
        {
            if (string.IsNullOrEmpty(accessToken))
                throw new ArgumentException($"{accessToken} is required");

            facebookClient.AccessToken = accessToken;

            if (string.IsNullOrEmpty(path))
                throw new ArgumentException($"{path} is required");

            try
            {
                return await facebookClient.GetTaskAsync<JsonObject>(path, parameters);
            }
            catch (FacebookOAuthException)
            {
                return null;
            }
        }

        public async Task<IList<IAlbum>> GetAlbumsByUserId(string accessToken, string userId)
        {
            var jsonResult = await Get(accessToken, $"{userId}/albums", new { fields = "id" });
            if (jsonResult == null)
                return await Task.FromResult<IList<IAlbum>>(new List<IAlbum>());

            return JsonConvert.DeserializeObject<IList<Album>>(
                jsonResult["data"].ToString()).Cast<IAlbum>().ToList();
        }

        public async Task<IList<IPhoto>> GetPhotosFromAlbum(string accessToken, string albumId)
        {
            var jsonResult = await Get(accessToken, $"{albumId}/photos", new { fields = "source, created_time, link" });
            if (jsonResult == null)
                return await Task.FromResult<IList<IPhoto>>(new List<IPhoto>());

            return JsonConvert.DeserializeObject<IList<Photo>>(
                jsonResult["data"].ToString()).Cast<IPhoto>().ToList();
        }

        public async Task<string> GetEmailByUserId(string accessToken, string userId)
        {
            var jsonResult = await Get(accessToken, $"{userId}", new { fields = "email" });
            if (jsonResult == null)
                return null;

            return jsonResult["email"].ToString();
        }

        public async Task<IProfileInfo> GetProfileInfoByUserId(string accessToken, string userId)
        {
            var jsonResult = await Get(accessToken, $"{userId}", new { fields = "name, link" });
            if (jsonResult == null)
                return null;

            return JsonConvert.DeserializeObject<IProfileInfo>(
                jsonResult.ToString(),
                new SocialNetworkEntityConverter<IProfileInfo, ProfileInfo>());
        }

        public async Task<IAvatar> GetAvatarByUserId(string accessToken, string userId)
        {
            var jsonResult = await Get(accessToken, $"{userId}/picture", new { fields = "url", redirect = false });
            if (jsonResult == null)
                return null;

            return JsonConvert.DeserializeObject<IAvatar>(
                jsonResult["data"].ToString(),
                new SocialNetworkEntityConverter<IAvatar, Avatar>());
        }
    }
}
