﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Facebook
{
    class SocialNetworkEntityConverter<IEntity, T> : CustomCreationConverter<IEntity>
        where T : IEntity, new()
    {
        public override IEntity Create(Type objectType)
        {
            return new T();
        }
    }
}
