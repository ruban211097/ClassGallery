﻿using Newtonsoft.Json;
using SocialNetworkService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Facebook.Entities
{
    class ProfileInfo : IProfileInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("link")]
        public string ProfileLink { get; set; }
    }
}
