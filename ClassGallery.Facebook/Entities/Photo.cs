﻿using Newtonsoft.Json;
using SocialNetworkService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Facebook.Entities
{
    public class Photo : IPhoto
    {
        [JsonProperty("created_time")]
        public string CreatedTime { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }
    }
}
