﻿using Newtonsoft.Json;
using SocialNetworkService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Facebook.Entities
{
    class Avatar : IAvatar
    {
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
