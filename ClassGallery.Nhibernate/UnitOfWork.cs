﻿using ClassGallery.Business.Interfaces;
using ClassGallery.Nhibernate.Helpers;
using NHibernate;
using System;
using System.Threading.Tasks;
using System.Web;

namespace ClassGallery.Nhibernate
{
    public class UnitOfWork : IUnitOfWork
    {
        private ITransaction transaction;

        #region HttpContext

        private ISession GetHttpContextSession =>
            (ISession)HttpContext.Current.Items["session"];

        private bool IsNullHttpContextSession =>
            GetHttpContextSession == null;

        private bool IsOpenHttpContextSession =>
            !IsNullHttpContextSession ? GetHttpContextSession.IsOpen : false;

        private ITransaction GetHttpContextTransaction => 
            Session.Transaction;
        
        #endregion

        public ISession Session
        {
            get
            {
                var current = HttpContext.Current;

                if (IsNullHttpContextSession || !IsOpenHttpContextSession)
                {
                    if (!current.Items.Contains("session"))
                        current.Items.Add("session", SessionHelper.OpenSession());
                    else
                        current.Items["session"] = SessionHelper.OpenSession();
                }

                return GetHttpContextSession;
            }
        }

        public Task BeginTransaction()
        {
            if (transaction == null)
            {
                if (!GetHttpContextTransaction.IsActive)
                    Session.BeginTransaction();

                transaction = GetHttpContextTransaction;
            }

            return Task.CompletedTask;
        }

        public async Task Commit()
        {
            try
            {
                if (transaction != null && transaction.IsActive)
                    await transaction.CommitAsync();
            }
            catch
            {
                if (transaction != null && transaction.IsActive)
                    await transaction.RollbackAsync();
            }
            finally
            {
               Dispose();
            }
        }

        public void Dispose()
        {
            Session.Dispose();
        }
    }
}
