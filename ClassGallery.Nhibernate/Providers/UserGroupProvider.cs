﻿using ClassGallery.Business.Entities;
using ClassGallery.Business.Interfaces;
using ClassGallery.Business.Providers;
using ClassGallery.Nhibernate.Providers.ServiceProviders;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClassGallery.Nhibernate.Providers
{
    public class UserGroupProvider : Provider<UserGroup>, IUserGroupProvider
    {
        public UserGroupProvider(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        public Task<IList<UserGroup>> GetByGroupId(string groupId)
        {
            return unitOfWork.Session.QueryOver<UserGroup>().Where(it => it.GroupId == groupId).ListAsync();
        }

        public Task<IList<string>> GetUserConfirmedGroupsId(string userId)
        {
            return unitOfWork.Session.QueryOver<UserGroup>()
                .Where(ug => ug.UserId == userId && ug.IsConfirmed)
                .Select(x => x.GroupId)
                .ListAsync<string>();
        }

        public Task<IList<UserGroup>> GetConfirmedUser(string groupId)
        {
            return unitOfWork.Session.QueryOver<UserGroup>().
                Where(it => it.GroupId == groupId && it.IsConfirmed == true).ListAsync();
        }

        public Task<IList<UserGroup>> GetConfirmedByGroupId(string groupId)
        {
            return unitOfWork.Session.QueryOver<UserGroup>().
                Where(it => it.GroupId == groupId && it.IsConfirmed).ListAsync();
        }

        public Task<IList<UserGroup>> GetIsNotConfirmedUser(string groupId)
        {
            return unitOfWork.Session.QueryOver<UserGroup>().
                Where(it => it.GroupId == groupId && it.IsConfirmed == false).ListAsync();
        }

        public async Task<UserGroup> GetByUserAndGroupId(string groupId, string userId)
        {
            return await unitOfWork.Session.QueryOver<UserGroup>().
                Where(it => it.GroupId == groupId && it.UserId == userId).SingleOrDefaultAsync();
        }
    }
}
