﻿using System.Threading.Tasks;
using ClassGallery.Business.Interfaces;
using ClassGallery.Business.Entities;
using ClassGallery.Business.Providers;
using ClassGallery.Nhibernate.Providers.ServiceProviders;
using System.Collections.Generic;
using System;
using System.Collections.ObjectModel;

namespace ClassGallery.Nhibernate.Providers
{
    public class GroupProvider : Provider<Group>, IGroupProvider
    {
        public GroupProvider(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        public Task<Group> GetById(string groupId)
        {
            return unitOfWork.Session.QueryOver<Group>().
                Where(group => group.Id == groupId).SingleOrDefaultAsync();
        }

        public Task<Group> GetByToken(string token)
        {
            return unitOfWork.Session.QueryOver<Group>().
                Where(group => group.InviteToken == token).SingleOrDefaultAsync();
        }

        public Task<IList<Group>> GetByIdList(IList<string> idList)
        {
            return unitOfWork.Session.QueryOver<Group>()
                .WhereRestrictionOn(g => g.Id)
                .IsIn(new Collection<string>(idList))
                .OrderBy(g => g.CreationDate)
                .Desc
                .ListAsync();
        }

        public async Task DeleteGroup(string groupId)
        {
            await Delete(groupId);
        }
    }
}
