﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ClassGallery.Business.Interfaces;
using ClassGallery.Business.Entities;
using ClassGallery.Business.Providers;
using System.Threading.Tasks;
using ClassGallery.Nhibernate.Providers.ServiceProviders;

namespace ClassGallery.Nhibernate.Providers
{
    public class UserProvider : Provider<User>, IUserProvider
    {
        public UserProvider(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        public async Task<User> GetByFacebookId(string facebookId)
        {
            return await unitOfWork.Session.QueryOver<User>().Where(u => u.FacebookId == facebookId).SingleOrDefaultAsync();
        }

        public async Task<User> GetByUserName(string userName)
        {
            return await unitOfWork.Session.QueryOver<User>().Where(u => u.UserName == userName).SingleOrDefaultAsync();
        }

    }
}
