﻿using ClassGallery.Business.Entities;
using FluentNHibernate.Mapping;

namespace ClassGallery.Nhibernate.Mappings
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("Users");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.FacebookId).Not.Nullable();
            Map(x => x.AccessToken).Not.Nullable();
            Map(x => x.UserName).Nullable();
        }
    }
}
