﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassGallery.Business.Entities;
using FluentNHibernate.Mapping;

namespace ClassGallery.Nhibernate.Mappings
{
    public class UserGroupMap : ClassMap<UserGroup>
    {
        public UserGroupMap()
        {
            Table("UserGroup");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.IsConfirmed).Not.Nullable();
            Map(x => x.GroupId).Not.Nullable();
            Map(x => x.UserId).Not.Nullable();
        }
    }
}
