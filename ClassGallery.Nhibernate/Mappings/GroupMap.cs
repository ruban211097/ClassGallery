﻿using ClassGallery.Business.Entities;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Nhibernate.Mappings
{
    public class GroupMap : ClassMap<Group>
    {
        public GroupMap()
        {
            Table("Groups");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Name).Not.Nullable();
            Map(x => x.Description).Nullable();
            Map(x => x.InviteToken).Not.Nullable();
            Map(x => x.CreationDate).Not.Nullable();
            Map(x => x.CreatorId).Not.Nullable();
        }
    }
}
