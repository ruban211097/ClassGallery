﻿using NHibernate;
using NHibernate.Cfg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Nhibernate.Helpers
{
    public class SessionHelper
    {
        private static volatile ISessionFactory factory;
        private static readonly object SyncRoot = new object();

        public static ISession OpenSession()
        {
            if (factory != null)            
                return factory.OpenSession();
           
            lock (SyncRoot)           
                if (factory == null)               
                    CreateFactory();

            return factory.OpenSession();
        }

        private static void CreateFactory()
        {
             factory = Configuration.SessionFactory;
        }
    }
}
