﻿using SocialNetworkService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialNetworkService.Providers
{
    public interface ISocialNetworkProvider
    {
        Task<IList<IPhoto>> GetPhotosFromAlbum(string accessToken, string albumId);

        Task<IList<IAlbum>> GetAlbumsByUserId(string accessToken, string userId);

        Task<string> GetEmailByUserId(string accessToken, string userId);

        Task<IProfileInfo> GetProfileInfoByUserId(string accessToken, string userId);

        Task<IAvatar> GetAvatarByUserId(string accessToken, string userId);
    }
}
