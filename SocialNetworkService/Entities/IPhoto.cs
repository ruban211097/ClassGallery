﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialNetworkService.Entities
{
    public interface IPhoto
    {
        string CreatedTime { get; set; }

        string Source { get; set; }

        string Link { get; set; }
    }
}
