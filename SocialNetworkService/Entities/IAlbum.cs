﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialNetworkService.Entities
{
    public interface IAlbum
    {
        string Id { get; set; }
    }
}
