﻿using SocialNetworkService.Entities;
using SocialNetworkService.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialNetworkService.Managers
{
    public class SocialNetworkManager
    {
        protected readonly ISocialNetworkProvider socialNetworkProvider;

        public SocialNetworkManager(ISocialNetworkProvider socialNetworkProvider)
        {
            this.socialNetworkProvider = socialNetworkProvider;
        }

        public async Task<IList<IPhoto>> GetAllPhotoBriefInfo(string accessToken, string userId)
        {
            var photoList = new List<IPhoto>();

            var albumList = await socialNetworkProvider.GetAlbumsByUserId(accessToken, userId);
            foreach (var album in albumList)
            {
                var photoListFromAlbum = (await socialNetworkProvider.GetPhotosFromAlbum(accessToken, album.Id)).ToList();

                photoList.AddRange(photoListFromAlbum);
            }

            return photoList;
        }

        public Task<string> GetEmailByUserId(string accessToken, string userId)
        {
            return socialNetworkProvider.GetEmailByUserId(accessToken, userId);
        }

        public Task<IProfileInfo> GetProfileInfoByUserId(string accessToken, string userId)
        {
            return socialNetworkProvider.GetProfileInfoByUserId(accessToken, userId);
        }

        public Task<IAvatar> GetAvatarByUserId(string accessToken, string userId)
        {
            return socialNetworkProvider.GetAvatarByUserId(accessToken, userId);
        }
    }
}
