﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ClassGallery.InstagramApi.ExtensionMethods;

namespace ClassGallery.InstagramApi
{
    public class InstagramClient
    {
        public string UserId { get; set; }
        public string AccessToken { get; set; }

        public virtual Task<JsonObject> GetTaskAsync<JsonObject>(string path, Dictionary<string,string> parameters,string requestMethod="GET", bool accessNeeded=false)
        {
            return Task.Run(() => {
                var requestParameters = parameters.ElementAt(0).GetPairString();
                for (int i = 1; i < parameters.Count; i++)
                    requestParameters += "&" + parameters.ElementAt(0).GetPairString();

                HttpWebRequest httpWebRequest;

                if (!accessNeeded)
                    httpWebRequest = (HttpWebRequest)WebRequest.Create("https://api.instagram.com/v1" +$"{path}?{requestParameters}");
                else
                    if(String.IsNullOrEmpty(requestParameters))
                        httpWebRequest = (HttpWebRequest)WebRequest.Create("https://api.instagram.com/v1" + $"{path}?access_token={AccessToken}");
                    else
                        httpWebRequest = (HttpWebRequest)WebRequest.Create("https://api.instagram.com/v1" + $"{path}?{requestParameters}&access_token={AccessToken}");

                httpWebRequest.ContentType = "text/json";
                httpWebRequest.Method = requestMethod;

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                JsonObject result;
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responce = streamReader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<JsonObject>(responce);
                }
                return result;
            });
        }

    }
}
