﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassGallery.InstagramApi.ExtensionMethods
{
    public static class InstagramExtensions
    {
        public static string GetPairString(this KeyValuePair<string,string> keyPair)
        {
            string param = keyPair.Key;
            if (!String.IsNullOrEmpty(keyPair.Value))
                param += $"={keyPair.Value}";

            return param;
        }
    }
}
