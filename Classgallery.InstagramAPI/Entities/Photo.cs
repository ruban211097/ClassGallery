﻿using Newtonsoft.Json;
using SocialNetworkService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.InstagramApi.Entities
{
    public class Image
    {
        public ResolutionDetails standard_resolution { get; set; }
    }

    public class ResolutionDetails
    {
        public string url { get; set; }
    }

    public class Media
    {
        public List<Image> images { get; set; }
    }

    public class Photo : IPhoto
    {
        public string CreatedTime { get; set; }

        public string Source { get; set; }

        public string Link { get; set; }
    }
}
