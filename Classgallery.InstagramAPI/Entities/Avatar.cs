﻿using Newtonsoft.Json;
using SocialNetworkService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.InstagramApi.Entities
{
    public class AvatarResult
    {
        public string profile_picture { get; set; }
    }


    public class Avatar : IAvatar
    {
        public string Url { get; set; }
    }
}
