﻿using Newtonsoft.Json;
using SocialNetworkService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.InstagramApi.Entities
{
    public class ProfileInfoResult
    {
        public ProfileCaption caption { get; set; }
        public string link { get; set; }
    }

    public class ProfileCaption
    {
        public ProfileFrom from { get; set; }
    }

    public class ProfileFrom
    {
        public string full_name { get; set; }
    }




    public class ProfileInfo : IProfileInfo
    {
        public string Name { get; set; }

        public string ProfileLink { get; set; }
    }
}
