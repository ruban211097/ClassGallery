﻿using SocialNetworkService.Entities;
using SocialNetworkService.Providers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using ClassGallery.InstagramApi;
using Com.CloudRail.SI;
using Com.CloudRail.SI.Services;
using Com.CloudRail.SI.ServiceCode.Commands.CodeRedirect;
using ClassGallery.InstagramApi.Entities;

namespace ClassGallery.InstagramApi.Providers
{
    public class InstagramProvider : ISocialNetworkProvider
    {
        protected readonly InstagramClient instagramClient;

        Instagram service = new Instagram(
            new LocalReceiver(44328),
            "f2820837e46a47f2b8dcdfdb1c671ff9",
            "6b545698bf944025a51a9584f6a7aed4",
            "http://localhost:44328/signin-instagram/",
            ""
        );

        public InstagramProvider(InstagramClient instagramClient)
        { 
            CloudRail.AppKey = "5b0c80511a59b150ae06e55a";
            this.instagramClient = instagramClient;
        }


        private async Task<JsonObject> Get<JsonObject>(string accessToken, string path, Dictionary<string,string> parameters = null)
        {
            if (string.IsNullOrEmpty(accessToken))
                throw new ArgumentException($"{accessToken} is required");

            instagramClient.AccessToken = accessToken;

            if (string.IsNullOrEmpty(path))
                throw new ArgumentException($"{path} is required");

            try
            {
                return await instagramClient.GetTaskAsync<JsonObject>(path, parameters);
            }
            catch
            {
                return default(JsonObject);
            }
        }

        public Task<IList<IAlbum>> GetAlbumsByUserId(string accessToken, string userId)
        {
            //No albums at instagram
            return null;
        }

        public async Task<IList<IPhoto>> GetPhotosFromAlbum(string accessToken, string photosCount)
        {
                var jsonResult = await Get<IList<Image>>(accessToken, "/users/self/media/recent/",
                    new Dictionary<string, string> { { "access_token", accessToken }, { "count", photosCount } });

            IList<IPhoto> photos = new List<IPhoto>();
            foreach (var photo in jsonResult)
                photos.Add(new Photo { Source= photo.standard_resolution.url });

            return photos;   
        }

        public Task<string> GetEmailByUserId(string accessToken, string userId)
        {
            return Task.Run(() =>
            {
                return service.GetEmail();
            });
        }


        public async Task<IAvatar> GetAvatarByUserId(string accessToken, string userId)
        {
            var jsonResult = await Get<AvatarResult>(accessToken, "/users/self/",
               new Dictionary<string, string> { { "access_token", accessToken } });

            IAvatar avatar = new Avatar() { Url = jsonResult.profile_picture };

            return avatar;

        }


        public async Task<IProfileInfo> GetProfileInfoByUserId(string accessToken, string userId)
        {
            var jsonResult = await Get<ProfileInfoResult>(accessToken, "/users/self/media/recent/",
               new Dictionary<string, string> { { "access_token", accessToken } });

            IProfileInfo profileInfo = new ProfileInfo() { Name = jsonResult.caption.from.full_name, ProfileLink = jsonResult.link };

            return profileInfo;
        }
    }
}
