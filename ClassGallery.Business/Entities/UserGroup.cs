﻿using System;
using ClassGallery.Business.Entities.Interfaces;

namespace ClassGallery.Business.Entities
{
    public class UserGroup:IEntity
    {
        public virtual string Id { get; set; }

        public virtual string UserId { get; set; }

        public virtual string GroupId { get; set; }

        public virtual bool IsConfirmed { get; set; }
    }
}
