﻿using ClassGallery.Business.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace ClassGallery.Business.Entities
{
    public class Group : IEntity
    {
        public virtual string Id { get; set; }

        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public virtual string InviteToken { get; set; }

        public virtual DateTime CreationDate { get; set; }

        public virtual string CreatorId { get; set; }
    }
}
