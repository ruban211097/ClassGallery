﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Business.Entities.Interfaces
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}
