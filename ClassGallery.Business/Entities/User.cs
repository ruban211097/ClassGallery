﻿using ClassGallery.Business.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using NHibernate.UserTypes; 

namespace ClassGallery.Business.Entities
{
    public class User : IEntity, IUser<string>
    {
        public virtual string Id { get; set; }

        public virtual string FacebookId { get; set; }

        public virtual string AccessToken { get; set; }

        public virtual string UserName { get; set; }
    }
}
