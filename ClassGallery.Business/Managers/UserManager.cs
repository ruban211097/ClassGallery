﻿using ClassGallery.Business.Entities;
using ClassGallery.Business.Providers;
using ClassGallery.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ClassGallery.Business.Managers.ServiceManagers;

namespace ClassGallery.Business.Managers
{
    public class UserManager : Manager<User, IUserProvider>
    {
        public UserManager(IUserProvider provider)
            : base(provider)
        {
        }

        public async Task CreateUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            await provider.SaveOrUpdate(user);
        }

        public async Task UpdateUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrEmpty(user.Id))
                throw new ArgumentException($"{nameof(user.Id)} is required.");

            await provider.SaveOrUpdate(user);
        }

        public async Task<IList<User>> GetList()
        {
            return await provider.GetList();
        }

        public async Task<User> GetByName(string userName)
        {
            return await provider.GetByUserName(userName);
        }

        public Task<User> GetByFacebookId(string facebookId)
        {
            return provider.GetByFacebookId(facebookId);
        }

        public Task DeleteUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return provider.Delete(user);
        }
    }
}
