﻿using ClassGallery.Business.Entities;
using ClassGallery.Business.Interfaces;
using ClassGallery.Business.Providers;
using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Threading.Tasks;
using ClassGallery.Business.Managers.ServiceManagers;

namespace ClassGallery.Business.Managers
{
    public class GroupManager : Manager<Group, IGroupProvider>
    {
        private IUserGroupProvider userGroupProvider;

        public GroupManager(IGroupProvider groupProvider, IUserGroupProvider userGroupProvider)
            : base(groupProvider)
        {
            this.userGroupProvider = userGroupProvider;
        }

        public async Task CreateGroup(Group group)
        {
            if (group == null)
                throw new ArgumentNullException(nameof(group));

            if (string.IsNullOrEmpty(group.Name))
                throw new ArgumentException($"{nameof(group.Name)} is required.");

            if (string.IsNullOrEmpty(group.Description))
                throw new ArgumentException($"{nameof(group.Description)} is required.");

            if (string.IsNullOrEmpty(group.CreatorId))
                throw new ArgumentException($"{nameof(group.CreatorId)} is required.");

            group.Id = provider.GenerateId();
            group.InviteToken = provider.GenerateTokenById(group);
            group.CreationDate = DateTime.Now;

            var userGroup = new UserGroup()
            {
                GroupId = group.Id,
                Id = provider.GenerateId(),
                IsConfirmed = true,
                UserId = group.CreatorId
            };

            await provider.SaveOrUpdate(group);
            await userGroupProvider.SaveOrUpdate(userGroup);

        }

        public async Task UpdateGroup(Group group)
        {
            if (group == null)
                throw new ArgumentNullException(nameof(group));

            if (string.IsNullOrEmpty(group.Id))
                throw new ArgumentException($"{nameof(group.Id)} is required.");

            if (string.IsNullOrEmpty(group.Name))
                throw new ArgumentException($"{nameof(group.Name)} is required.");

            await provider.SaveOrUpdate(group);
        }

        public async Task RenameGroup(Group group, string newNameGroup)
        {
            if (group == null)
                throw new ArgumentNullException(nameof(group));

            if (string.IsNullOrEmpty(group.Id))
                throw new ArgumentException($"{nameof(group.Id)} is required.");

            group.Name = newNameGroup;

            await provider.SaveOrUpdate(group);
        }

        public async Task DeleteGroup(string groupId)
        {
            if (String.IsNullOrEmpty(groupId))
                throw new ArgumentException($"{nameof(groupId)} is required.");

            var userGroups = await userGroupProvider.GetListById(groupId);

            if (userGroups == null)
                throw new ArgumentNullException(nameof(userGroups));

            foreach (var userGroup in userGroups)            
                await userGroupProvider.Delete(userGroup);
            
            await provider.DeleteGroup(groupId);
        }

        public Task<IList<Group>> GetList()
        {
            return provider.GetList();
        }

        public Task<Group> GetByToken(string token)
        {
            return provider.GetByToken(token);
        }

        public Task<IList<Group>> GetByIdList(IList<string> idList)
        {
            if (idList == null)
                throw new ArgumentNullException(nameof(idList));

            if (idList.Count == 0)
                return Task.FromResult<IList<Group>>(new List<Group>());

            return provider.GetByIdList(idList);
        }
    }
}
