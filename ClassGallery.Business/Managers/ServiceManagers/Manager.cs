﻿using ClassGallery.Business.Entities.Interfaces;
using ClassGallery.Business.Providers;
using ClassGallery.Business.Interfaces;
using System;
using System.Threading.Tasks;
using ClassGallery.Business.Providers.ServiceProviders;

namespace ClassGallery.Business.Managers.ServiceManagers
{
    public abstract class Manager<T, TProvider>
        where T : IEntity
        where TProvider : IProvider<T>
    {
        protected readonly TProvider provider;

        public Manager(TProvider provider)
        {
            this.provider = provider;
        }

        public virtual Task<T> GetById(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentException(nameof(id));

            return provider.Get(id);
        }
    }
}
