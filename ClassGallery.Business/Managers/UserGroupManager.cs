﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassGallery.Business.Entities;
using ClassGallery.Business.Interfaces;
using ClassGallery.Business.Managers.ServiceManagers;
using ClassGallery.Business.Providers;

namespace ClassGallery.Business.Managers
{
    public class UserGroupManager : Manager<UserGroup, IUserGroupProvider>
    {
        public UserGroupManager(IUserGroupProvider provider)
            : base(provider)
        {
        }

        public async Task<UserGroup> CreateUserGroup(UserGroup userGroup)
        {
            if (userGroup == null)
                throw new ArgumentNullException(nameof(userGroup));

            userGroup = await provider.SaveOrUpdate(userGroup);

            return userGroup;
        }

        public async Task UpdateUserGroup(UserGroup userGroup)
        {
            if (userGroup == null)
                throw new ArgumentNullException(nameof(userGroup));

            if (string.IsNullOrEmpty(userGroup.Id))
                throw new ArgumentException($"{nameof(userGroup.Id)} is required.");

            await provider.SaveOrUpdate(userGroup);
        }

        public async Task<IList<UserGroup>> GetByGroupId(string groupId)
        {
            if (string.IsNullOrEmpty(groupId))
                throw new ArgumentException($"{nameof(groupId)} is required.");

            return await provider.GetByGroupId(groupId);
        }

        public async Task DeleteGroupUser(UserGroup groupUser)
        {
            await provider.Delete(groupUser.Id);
        }

        public Task<IList<string>> GetUserConfirmedGroupsId(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ArgumentException(nameof(userId));

            return provider.GetUserConfirmedGroupsId(userId);
        }

        public Task<IList<UserGroup>> GetListIsNotConfirmedUsers(string groupId)
        {
            if (string.IsNullOrEmpty(groupId))
                throw new ArgumentException(nameof(groupId));

            return provider.GetIsNotConfirmedUser(groupId);
        }

        public Task<IList<UserGroup>> GetConfirmedByGroupId(string groupId)
        {
            if (string.IsNullOrEmpty(groupId))
                throw new ArgumentException(nameof(groupId));

            return provider.GetConfirmedByGroupId(groupId);
        }

        public async Task<UserGroup> GetByUserAndGroupId(string groupId, string userId)
        {
            if (string.IsNullOrEmpty(groupId))
                throw new ArgumentException(nameof(groupId));

            if (string.IsNullOrEmpty(userId))
                throw new ArgumentException(nameof(userId));

            return await provider.GetByUserAndGroupId(groupId, userId);
        }
    }
}
