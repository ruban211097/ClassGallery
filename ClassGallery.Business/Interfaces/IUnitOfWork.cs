﻿using System;
using NHibernate;
using System.Threading.Tasks;

namespace ClassGallery.Business.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        Task Commit();

        Task BeginTransaction();

        ISession Session { get; }
    }
}
