﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ClassGallery.Business.Entities;
using ClassGallery.Business.Providers.ServiceProviders;

namespace ClassGallery.Business.Providers
{
    public interface IUserProvider : IProvider<User>
    {
        Task<User> GetByFacebookId(string facebookId);

        Task<User> GetByUserName(string userName);
    }
}
