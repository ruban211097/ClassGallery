﻿using ClassGallery.Business.Entities;
using ClassGallery.Business.Providers.ServiceProviders;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClassGallery.Business.Providers
{
    public interface IUserGroupProvider : IProvider<UserGroup>
    {
        Task<IList<UserGroup>> GetByGroupId(string groupId);
        Task<IList<string>> GetUserConfirmedGroupsId(string userId);
        Task<IList<UserGroup>> GetIsNotConfirmedUser(string groupId);
        Task<IList<UserGroup>> GetConfirmedByGroupId(string groupId);
        Task<UserGroup> GetByUserAndGroupId(string groupId, string userId);
    }
}
