﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ClassGallery.Business.Entities;
using ClassGallery.Business.Providers.ServiceProviders;

namespace ClassGallery.Business.Providers
{
    public interface IGroupProvider : IProvider<Group>
    {
        Task<Group> GetByToken(string token);
        Task<Group> GetById(string groupId);
        Task DeleteGroup(string groupId);
        Task<IList<Group>> GetByIdList(IList<string> idList);
    }
}
