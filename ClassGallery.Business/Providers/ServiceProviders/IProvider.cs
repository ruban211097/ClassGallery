﻿using ClassGallery.Business.Entities;
using ClassGallery.Business.Entities.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClassGallery.Business.Providers.ServiceProviders
{
    public interface IProvider<T> where T : IEntity
    {
        string GenerateId();
        string GenerateTokenById(T entity);
        Task<T> Create(T entity);
        Task<T> Get(string id);
        Task<T> SaveOrUpdate(T entity);
        Task Delete(string id);
        Task Delete(T entity);
        Task<IList<T>> GetList();
        Task<IList<T>> GetListById(string id);
    }
}
