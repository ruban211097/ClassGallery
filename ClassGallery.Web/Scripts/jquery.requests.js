﻿$(document).on("visibilitychange", handleVisibilityChange);

$('tr').on('click', saceClickIdTagTr);

function saceClickIdTagTr() {
    let trIdAtribute = $(this).attr("id");
    localStorage["valueAtribute"] = trIdAtribute;
};

function handleVisibilityChange() {
    if (document.hidden !== undefined) {
        if (!(document.hidden)) {
     
            $.ajax({
                url: "/Group/ShowRequestsJson" + window.location.search,
                HttpMethod: "Get",
                dataType: "json",
                success: successResultUpdateList
            });
        }
    }
    else {
        alert("The browser does not support the Page Visibility API");
    }
}

function successResultUpdateList(data) {
    $("#load").css("display", "flex");

    let trLength = $("#tableBody tr");

    if (trLength.length != data.length || data.length == 0) {

        let resultTag = $("#tableBody");

        resultTag.empty();

        if (data.length > 0) {

            for (let i = 0; i < data.length; i++) {

                let vue = new Vue({
                    el: "#template-form",
                    data: {
                        idForm: data[i].UserGroupId,
                        userName: data[i].UserName,
                        userGroupId: data[i].UserGroupId
                    }
                })

                let templateForm = $('#template-form').contents().clone();

                resultTag.append(templateForm);
            }
        }
        else {
            resultTag.append('<tr><td colspan="2"><h1>Нет доступных заявок!</h2></td></tr>');
        }
    }

    $("#load").css("display", "none");
}

function OnSuccessOperationUser(data) {
    let atribute = localStorage["valueAtribute"];

    if (data == "User is not defined") 
        alert("User is not defined! We will remove this user from the table.");
    
    $("#" + atribute).remove();

    let trLength = $("#tableBody tr");

    if (trLength.length == 0) {
        $("#tableBody").append('<tr><td colspan="2"><h1>Нет доступных заявок!</h2></td></tr>');
    }
};

function OnFailureOperationUser(request, error) {
    alert("This is the OnFailure Callback, please refresh the page!");
}