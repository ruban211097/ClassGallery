﻿$(".container-list-group").on("click", "input[name=\"copy-link\"]", copyLink);

function copyLink() {
    let url = $(this).data("link");
    console.log(url);
    let tmp = document.createElement("INPUT");
    let focus = document.activeElement; 
    tmp.value = url;
    document.body.appendChild(tmp);
    tmp.select();
    document.execCommand('copy');
    document.body.removeChild(tmp);
    focus.focus();
}