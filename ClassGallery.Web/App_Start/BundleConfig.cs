﻿using System.Web;
using System.Web.Optimization;

namespace ClassGallery.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/templates").Include(
                "~/Scripts/vue.js", 
                "~/Scripts/vue.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/menu").Include(
                "~/Scripts/menu.jquery.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/bundles/sendLetters").Include(
                      "~/Scripts/sendLetter.js"));

            bundles.Add(new ScriptBundle("~/bundles/copyLink").Include(
                    "~/Scripts/copyLink.js"));

            bundles.Add(new ScriptBundle("~/jquery/ajax").Include(
                "~/Scripts/jquery-{version}.js", 
                "~/Scripts/jquery.unobtrusive-ajax.min.js"));

            bundles.Add(new ScriptBundle("~/jquery/requests").Include(
                "~/Scripts/jquery.requests.js"));
        }
    }
}
