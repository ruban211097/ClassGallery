using Unity;
using ClassGallery.Business.Providers;
using ClassGallery.Business.Interfaces;
using ClassGallery.Mvc.Identity;
using Facebook;
using Unity.Injection;
using SocialNetworkService.Providers;

namespace ClassGallery.Web.DependencyInjection
{
    public static class UnityRegistrations
    {
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IUserProvider, Nhibernate.Providers.UserProvider>();
            container.RegisterType<IGroupProvider, Nhibernate.Providers.GroupProvider>();
            container.RegisterType<IUserGroupProvider, Nhibernate.Providers.UserGroupProvider>();
            container.RegisterType<IdentityStore, IdentityStore>();
            container.RegisterType<FacebookClient>(new InjectionConstructor());
            container.RegisterType<ISocialNetworkProvider, Facebook.Providers.FacebookProvider>();
            container.RegisterType<IUnitOfWork, Nhibernate.UnitOfWork>();
        }
    }
}