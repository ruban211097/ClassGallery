using System;
using System.Linq;
using System.Web.Mvc;
using Unity;
using Unity.AspNet.Mvc;

namespace ClassGallery.Web.DependencyInjection
{
    public static class UnityConfig
    {
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            UnityRegistrations.RegisterTypes(container);
            return container;
        });

        public static IUnityContainer Container => container.Value;
    }
}