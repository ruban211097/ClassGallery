﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using ClassGallery.Mvc.Identity;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Microsoft.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Facebook;
using Aminjam.Owin.Security.Instagram;
using System.Web.Configuration;
using ClassGallery.Mvc.Identity.Managers;

[assembly: OwinStartup(typeof(ClassGallery.Web.Startup))]

namespace ClassGallery.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext(() => new IdentityUserManager(
                DependencyResolver.Current.GetService<IdentityStore>()));

            app.CreatePerOwinContext<IdentitySignInManager>((options, context) =>
                new IdentitySignInManager(context.GetUserManager<IdentityUserManager>(), context.Authentication));

            app.UseCookieAuthentication(new CookieAuthenticationOptions()
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/facebook/facebookLogin/")
            });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            var webConfiquration = WebConfigurationManager.AppSettings;

            app.UseFacebookAuthentication(new FacebookAuthenticationOptions
            {
                AppId = webConfiquration["AppIdFacebook"],
                AppSecret = webConfiquration["AppSecretFacebook"],
                SignInAsAuthenticationType = app.GetDefaultSignInAsAuthenticationType(),
                Scope = { "public_profile", "email", "user_photos", "user_link" },
                Provider = new FacebookAuthenticationProvider()
                {
                    OnAuthenticated = (context) =>
                    {
                        context.Identity.AddClaim(new Claim("urn:facebook:access_token", context.AccessToken, "string",
                            "Facebook"));
                        return Task.FromResult(0);
                    }
                }
            });

        }
    }
}