﻿using System.Web.Mvc;
using System.Web.Routing;

namespace ClassGallery.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "GroupUsers",
                url: "Group/GroupUsers/{groupId}",
                defaults: new { controller = "Group", action = "GroupUsers", groupId = @"\w+" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
