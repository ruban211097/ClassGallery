﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ClassGallery.Mvc.ExtensionMethods
{
    public static class InviteLink
    {
        public static string GetInviteLink(this UrlHelper url, string token, HttpRequestBase request)
        {
            return url.Action("AddUserToGroup", "Group", new { token = token}, request.Url.Scheme);
        }
    }
}
