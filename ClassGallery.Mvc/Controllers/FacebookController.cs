﻿using System.Net;
using System.Threading.Tasks;
using ClassGallery.Business.Managers;
using ClassGallery.Mvc.ViewModels;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Web;
using System.Web.UI.WebControls;
using ClassGallery.Business.Entities;
using ClassGallery.Mvc.Identity;
using Microsoft.Owin;
using Owin;
using Facebook;

namespace ClassGallery.Mvc.Controllers
{
    public class FacebookController : BaseController
    {
        private readonly UserGroupManager userGroupManager;
        private readonly UserManager userManager;

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        public FacebookController(UserGroupManager userGroupManager,UserManager userManager)
        {
            this.userGroupManager = userGroupManager;
            this.userManager = userManager;
        }

        public ActionResult FacebookLogin(string returnUrl)
        {
            var properties = new AuthenticationProperties
            {
                RedirectUri = Url.Action("FacebookLoginCallback", "Facebook", new { returnUrl })
            };

            properties.Dictionary["XsrfKey"] = "XsrfKey";

            HttpContext.GetOwinContext().Authentication.Challenge(properties, "Facebook");

            return new EmptyResult();
        }

        public async Task<ActionResult> FacebookLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();

            if (loginInfo == null)
                return RedirectToAction("Index", "Home");

            var result = await IdentitySignInManager.ExternalSignInAsync(loginInfo, false);

            switch (result)
            {
                case SignInStatus.Success:
                    {
                        var user = await userManager.GetByFacebookId(loginInfo.Login.ProviderKey);
                        user.AccessToken = loginInfo.ExternalIdentity.FindFirst("urn:facebook:access_token").Value;

                        await userManager.UpdateUser(user);
                        break;
                    }
                case SignInStatus.Failure:
                    {
                        var user = new User()
                        {
                            AccessToken = loginInfo.ExternalIdentity.FindFirst("urn:facebook:access_token").Value,
                            FacebookId = loginInfo.Login.ProviderKey,
                            UserName = loginInfo.DefaultUserName,
                        };
                        await IdentityUserManager.CreateAsync(user);
                        await IdentitySignInManager.SignInAsync(user, false, false);
                        break;
                    }
            }

            if (string.IsNullOrEmpty(returnUrl))
                return RedirectToAction("Index", "Home");

            return Redirect(returnUrl);
        }
    }
}
