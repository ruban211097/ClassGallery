﻿using AutoMapper;
using ClassGallery.Business.Entities;
using ClassGallery.Business.Managers;
using ClassGallery.Mvc.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ClassGallery.Mvc.Filters;

namespace ClassGallery.Mvc.Controllers
{
    [Culture]
    public class HomeController : Controller
    {
        private readonly GroupManager groupManager;
        private readonly UserGroupManager userGroupManager;

        public HomeController(GroupManager groupManager, UserGroupManager userGroupManager)
        {
            this.groupManager = groupManager;
            this.userGroupManager = userGroupManager;
        }
        
        public async Task<ActionResult> Index()
        {
            var groupList = await groupManager.GetList();

            groupList?.OrderByDescending(it => it.CreationDate).ToList();

            var groupViewModels = Mapper.Map<IEnumerable<Group>, List<GroupViewModel>>(groupList);


            foreach (var model in groupViewModels)
            {
                model.InviteLink = Url.Action("AddUserToGroup", "Group", new { token = model.InviteLink }, Request.Url.Scheme);
                var listIsNotConfirmedUsers = (await userGroupManager.GetListIsNotConfirmedUsers(model.Id)).ToList();
                model.RequestsNumber = listIsNotConfirmedUsers.Capacity;
                if (User.Identity.IsAuthenticated)
                {
                    var userGroup = await userGroupManager.GetByUserAndGroupId(model.Id, User.Identity.GetUserId());
                    model.IsInGroup = userGroup != null && userGroup.IsConfirmed;
                }
            }
            
            return View(groupViewModels);
        }

        public ActionResult ChangeCulture(string language)
        {
            List<string> cultures = new List<string> { "ru", "en" };
            if (!cultures.Contains(language))
                language = "ru";

            HttpCookie cookie = Request.Cookies["language"];
            if (cookie == null)
            {
                cookie = new HttpCookie("language")
                {
                    HttpOnly = false,
                    Value = language,
                    Expires = DateTime.Now.AddYears(1)
                };
            }
            else
                cookie.Value = language;

            Response.Cookies.Add(cookie);

            string returnUrl = Request.UrlReferrer.AbsolutePath;
            return Redirect(returnUrl);
        }
    }
}