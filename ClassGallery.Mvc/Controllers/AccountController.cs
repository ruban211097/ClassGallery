﻿using System.Linq;
using ClassGallery.Mvc.ViewModels;
using System.Web.Mvc;
using ClassGallery.Business.Entities;
using ClassGallery.Business.Managers;
using System.Threading.Tasks;
using System.Web;
using Facebook;
using ClassGallery.Facebook;
using ClassGallery.Mvc.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ClassGallery.Nhibernate.Providers;

namespace ClassGallery.Mvc.Controllers
{
    public class AccountController : BaseController
    {
        private readonly UserManager userManager;

        public AccountController(UserManager userManager)
        {
            this.userManager = userManager;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await IdentitySignInManager.PasswordSignInAsync(model.UserName, model.Password, false, false);

                switch (result)
                {
                    case SignInStatus.Success:
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    case SignInStatus.Failure:
                        {
                            ModelState.AddModelError("", "Wrong login or password!");
                            break;
                        }
                    case SignInStatus.LockedOut:
                        {
                            ModelState.AddModelError("", "Your account is blocked!");
                            break;
                        }
                    default:
                        {
                            ModelState.AddModelError("", "Account server error. Please, contact support team.");
                            break;
                        }
                }
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            IdentitySignInManager.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}