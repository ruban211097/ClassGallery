﻿using AutoMapper;
using ClassGallery.Business.Entities;
using ClassGallery.Business.Managers;
using ClassGallery.Mvc.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ClassGallery.Mvc.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult NotFound()
        {
            ErrorViewModel errorViewModel = new ErrorViewModel()
            {
                StatusCode = Response.StatusCode = 404,
                StatusMesage = "Not Found"
            };

            return View("Errors", errorViewModel);
        }

        public ActionResult BadRequest()
        {
            ErrorViewModel errorViewModel = new ErrorViewModel()
            {
                StatusCode = Response.StatusCode = 400,
                StatusMesage = "Bad Request"
            };
            return View("Errors", errorViewModel);
        }

        public ActionResult Forbidden()
        {
            ErrorViewModel errorViewModel = new ErrorViewModel()
            {
                StatusCode = Response.StatusCode = 403,
                StatusMesage = "Forbidden"
            };
            return View("Errors", errorViewModel);
        }

        public ActionResult InternalServerError()
        {
            ErrorViewModel errorViewModel = new ErrorViewModel()
            {
                StatusCode = Response.StatusCode = 500,
                StatusMesage = "Internal Server Error"
            };
            return View("Errors", errorViewModel);
        }

        public ActionResult GatewayTimeout()
        {
            ErrorViewModel errorViewModel = new ErrorViewModel()
            {
                StatusCode = Response.StatusCode = 504,
                StatusMesage = "Gateway Timeout"
            };
            return View("Errors", errorViewModel);
        }
    }
}