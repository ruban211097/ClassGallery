﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using ClassGallery.Mvc.ViewModels;
using System.Web.Mvc;
using ClassGallery.Business.Entities;
using ClassGallery.Business.Managers;
using System.Threading.Tasks;
using System.Web;
using ClassGallery.Mvc.Services;
using Microsoft.AspNet.Identity;
using SocialNetworkService.Managers;
using System;
using ClassGallery.Mvc.Identity.Managers;
using SocialNetworkService.Entities;
using ClassGallery.Mvc.Identity;
using ClassGallery.Facebook.Entities;
using System.Collections;
using ClassGallery.Mvc.Filters;
using ClassGallery.Mvc.ExtensionMethods;

namespace ClassGallery.Mvc.Controllers
{
    [Culture]
    [Authorize]
    public class GroupController : BaseController
    {
        private readonly GroupManager groupManager;
        private readonly UserManager userManager;
        private readonly UserGroupManager groupMemberManager;
        private readonly SocialNetworkManager socialNetworkManager;
        private readonly EmailService emailService;

        public GroupController(GroupManager groupManager, UserManager userManager,
            UserGroupManager userGroupManager, SocialNetworkManager socialNetworkManager, EmailService emailService)
        {
            this.groupManager = groupManager;
            this.groupMemberManager = userGroupManager;
            this.userManager = userManager;
            this.socialNetworkManager = socialNetworkManager;
            this.emailService = emailService;
        }

        public async Task<ActionResult> GroupList()
        {
            var useGroupIdList = await groupMemberManager.GetUserConfirmedGroupsId(User.Identity.GetUserId());

            var groupList = await groupManager.GetByIdList(useGroupIdList);

            var groupViewModels = Mapper.Map<List<GroupViewModel>>(groupList);

            foreach (var model in groupViewModels)
                model.InviteLink = Url.GetInviteLink(model.InviteLink, Request);

            return View(groupViewModels);
        }

        [AllowAnonymous]
        public async Task<ActionResult> GroupUsers(string groupId)
        {
            if (string.IsNullOrEmpty(groupId))
                return HttpNotFound();

            var group = await groupManager.GetById(groupId);

            if (group == null)
                return HttpNotFound();

            var groupMembersList = await groupMemberManager.GetConfirmedByGroupId(groupId);

            var groupUsersViewModel = new List<GroupMember>();

            foreach (var groupMember in groupMembersList)
            {
                var socialNetworkUser = await userManager.GetById(groupMember.UserId);

                if (socialNetworkUser != null)
                {
                    var user = await userManager.GetById(groupMember.UserId);

                    var profileInfo = await socialNetworkManager.GetProfileInfoByUserId(socialNetworkUser.AccessToken, user.FacebookId);

                    var profileAvatar = await socialNetworkManager.GetAvatarByUserId(socialNetworkUser.AccessToken, user.FacebookId);

                    groupUsersViewModel.Add(new GroupMember
                    {
                        ImageUrl = profileAvatar.Url,
                        Name = profileInfo.Name,
                        ProfileUrl = profileInfo.ProfileLink
                    });
                }
            }

            GroupPageViewModel groupPageViewModel = new GroupPageViewModel
            {
                briefUsersInfo = groupUsersViewModel,
                InviteLink = Url.GetInviteLink(group.InviteToken, Request),
                Photos = await GetGallery(groupId)
            };

            return View(groupPageViewModel);
        }

        #region Requests

        public async Task<ActionResult> Requests(string groupId)
        {
            if (string.IsNullOrEmpty(groupId))
                return HttpNotFound();

            var currentUser = await IdentityUserManager.FindByIdAsync(User.Identity.GetUserId());

            var currentUserGroup = await groupMemberManager.GetByUserAndGroupId(groupId, currentUser.Id);

            if (currentUserGroup == null)
                return HttpNotFound();

            if (!currentUserGroup.IsConfirmed)
                return new HttpNotFoundResult();

            var listRequests = await UserRequests(groupId);

            return View(listRequests);
        }

        public async Task<JsonResult> ShowRequestsJson(string groupId)
        {
            if (string.IsNullOrEmpty(groupId))
                return Json("Id is empty");

            var currentUser = await IdentityUserManager.FindByIdAsync(User.Identity.GetUserId());

            var currentUserGroup = await groupMemberManager.GetByUserAndGroupId(groupId, currentUser.Id);

            if (currentUserGroup == null)
                return Json("No access");

            var listRequests = await UserRequests(groupId);

            return Json(listRequests, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ConfirmRequest(UserRequestViewModel model)
        {
            if (!ModelState.IsValid)
                throw new Exception("The model is not valid!");

            var groupMember = await groupMemberManager.GetById(model.UserGroupId);

            if (groupMember == null)
                return Json("User is not defined");

            if (!groupMember.IsConfirmed)
            {

                var group = await groupManager.GetById(groupMember.GroupId);

                var currentUser = await IdentityUserManager.FindByIdAsync(User.Identity.GetUserId());

                var currentUserGroup = await groupMemberManager.GetByUserAndGroupId(group.Id, currentUser.Id);

                if (currentUserGroup == null)
                    return Json("No access");

                var user = await userManager.GetById(groupMember.UserId);

                groupMember.IsConfirmed = true;

                await groupMemberManager.UpdateUserGroup(groupMember);

                var receiver = await socialNetworkManager.GetEmailByUserId(user.AccessToken, user.FacebookId);

                if (receiver == null)
                    return Json("User not authorized");

                var subject = $"Adding to group was completed";
                var callbackUrl = Url.Action("GroupList", "Group", null, Request.Url.Scheme);
                var body = $"You have been added to the <a href=\"{callbackUrl}\">{group.Name}</a> group.";
                await emailService.SendLetterAsync(receiver, subject, body);


                return Json("Success");
            }
            else
                return Json("User already confirmed.");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> RefuseRequest(UserRequestViewModel model)
        {
            if (!ModelState.IsValid)
                throw new Exception("The model is not valid!");

            var groupMember = await groupMemberManager.GetById(model.UserGroupId);

            if (groupMember == null)
                return Json("User is not defined");

            if (groupMember.IsConfirmed)
                return Json("User already confirmed.");

            var currentUser = await IdentityUserManager.FindByIdAsync(User.Identity.GetUserId());

            var currentUserGroup = await groupMemberManager.GetByUserAndGroupId(groupMember.GroupId, currentUser.Id);

            if (currentUserGroup == null)
                return Json("No access");

            await groupMemberManager.DeleteGroupUser(groupMember);

            return Json("Success");
        }

        #endregion

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(GroupViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var group = new Group()
            {
                Name = model.Name,
                CreatorId = User.Identity.GetUserId(),
                Description = model.Description
            };

            await groupManager.CreateGroup(group);

            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> Rename(string groupId)
        {
            if (String.IsNullOrEmpty(groupId))
                return HttpNotFound();

            var group = await groupManager.GetById(groupId);

            if (group == null)
                return HttpNotFound();

            var groupViewModel = new GroupViewModel()
            {
                Id = group.Id,
                Name = group.Name
            };

            return View(groupViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Rename(GroupViewModel model)
        {
            if (!ModelState.IsValid)
                return View();

            var group = await groupManager.GetById(model.Id);

            if (group == null)
                return HttpNotFound();

            if (String.Equals(group.Name, model.Name) )
                ModelState.AddModelError("Name", "We need a new name!");
            else
                await groupManager.RenameGroup(group, model.Name);

            return PartialView("Success");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(string groupId)
        {
            var group = await groupManager.GetById(groupId);

            if (group == null)
                return HttpNotFound();

            if (group.CreatorId != User.Identity.GetUserId())
                return new HttpUnauthorizedResult();

            await groupManager.DeleteGroup(groupId);

            return PartialView("Success");
        }

        [AllowAnonymous]
        public async Task<ActionResult> AddUserToGroup(string token)
        {
            if (string.IsNullOrEmpty(token))
                return HttpNotFound();

            var group = await groupManager.GetByToken(token);

            if (group == null)
                return HttpNotFound();

            if (User.Identity.IsAuthenticated)
            {
                var currentUser = await IdentityUserManager.FindByIdAsync(User.Identity.GetUserId());

                var groupMember = await groupMemberManager.GetByUserAndGroupId(group.Id, currentUser.Id);

                if (groupMember != null)
                {
                    if (!groupMember.IsConfirmed)
                        return View("Waiting");

                    return RedirectToAction("GroupUsers", "Group", new { groupId = group.Id });
                }
            }

            if (Session["InviteLink"] == null || Session["InviteLink"].ToString() != Request.Url.AbsoluteUri)
            {
                Session["InviteLink"] = Request.Url.AbsoluteUri;
                return RedirectToAction("GroupUsers", "Group", new { groupId = group.Id });
            }

            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("FacebookLogin", "Facebook", new { returnUrl = Request.Url });

            Session["InviteLink"] = null;

            var newGroupMember = await groupMemberManager.CreateUserGroup(new UserGroup()
            {
                GroupId = group.Id,
                UserId = User.Identity.GetUserId()
            });

            return View("Waiting");
        }

        #region HelperMethods

        private async Task<IList<UserRequestViewModel>> UserRequests(string groupId)
        {
            var userGroups = await groupMemberManager.GetListIsNotConfirmedUsers(groupId);

            if (userGroups == null)
                throw new HttpException("Group is not found!");

            var users = new List<User>();
            foreach (var userGroup in userGroups)
            {
                users.Add(await userManager.GetById(userGroup.UserId));
            }

            var model = new List<UserRequestViewModel>();
            foreach (var user in users)
            {
                var userGroup = userGroups.First(it => it.UserId == user.Id);
                model.Add(new UserRequestViewModel() { UserName = user.UserName, IsConfirmed = false, UserGroupId = userGroup.Id });
            }

            return model;
        }


        public async Task<List<IPhoto>> GetGallery(string groupId)
        {
            if (string.IsNullOrEmpty(groupId))
                return await Task.FromResult(new List<IPhoto>());

            var photoList = new List<IPhoto>();

            var groupMembers = await groupMemberManager.GetConfirmedByGroupId(groupId);
            foreach (var groupMember in groupMembers)
            {
                var userPofile = await userManager.GetById(groupMember.UserId);
                var userPhotos = await socialNetworkManager
                    .GetAllPhotoBriefInfo(userPofile.AccessToken, userPofile.FacebookId);
                photoList.AddRange(userPhotos);
            }

            return photoList;
        }
        #endregion
    }
}