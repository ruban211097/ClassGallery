﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ClassGallery.Mvc.Logger
{
    public class Logger:IHttpModule
    {
        public void Init(HttpApplication app)
        {
            app.LogRequest += Log;
        }

        private void Log(object sender, EventArgs e)
        {
            var app = (HttpApplication) sender;
            var context = app.Context;
            var requestDate = context.Timestamp;
            var dateString = $"{requestDate.Day}.{requestDate.Month}.{requestDate.Year}";
            var timeString = $"{requestDate.Hour}:{requestDate.Minute}:{requestDate.Second}";
            var exceptionString = "";
            if (context.AllErrors != null && context.AllErrors.Length != 0)
            {
                foreach (Exception exp in context.AllErrors)
                {
                    exceptionString += exp.Message;
                    if (exp.InnerException != null)
                        exceptionString += exp.InnerException.Message;
                }
            }
            /*
           using (var stream =
                new StreamWriter(
                    $"{HttpContext.Current.Server.MapPath($"/App_Data/Logs/{dateString}.txt")}",
                    true))
            {
                stream.WriteLine(
                    $"{dateString,-10}{timeString,-8}{context.Request.UserHostAddress,15} {context.Request.Url,-70}{context.Response.StatusCode,4} {exceptionString}");
            }*/
        }

        public void Dispose()
        {
            
        }
    }
}
