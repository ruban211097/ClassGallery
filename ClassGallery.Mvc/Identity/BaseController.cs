﻿using System.Web;
using System.Web.Mvc;
using ClassGallery.Mvc.Identity.Managers;
using Microsoft.AspNet.Identity.Owin;

namespace ClassGallery.Mvc.Identity
{
    public class BaseController : Controller
    {
        public IdentityUserManager IdentityUserManager => 
            HttpContext.GetOwinContext().GetUserManager<IdentityUserManager>();

        public IdentitySignInManager IdentitySignInManager => 
            HttpContext.GetOwinContext().Get<IdentitySignInManager>();
    }
}
