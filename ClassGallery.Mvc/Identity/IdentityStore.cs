﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using ClassGallery.Business.Entities;
using ClassGallery.Business.Managers;
using Microsoft.AspNet.Identity;

namespace ClassGallery.Mvc.Identity
{
    public class IdentityStore : IUserStore<User, string>, IUserLoginStore<User, string>
    {
        private UserManager userManager;

        public IdentityStore(UserManager userManager)
        {
            this.userManager = userManager;
        }

        #region IUserStore<User, string>

        public Task CreateAsync(User user)
        {
            return Task.Run(() => userManager.CreateUser(user));
        }

        public Task DeleteAsync(User user)
        {
            return Task.Run(() => userManager.DeleteUser(user));
        }

        public Task<User> FindByIdAsync(string userId)
        {
            return userManager.GetById(userId);
        }

        public Task UpdateAsync(User user)
        {
            return Task.Run(() => userManager.UpdateUser(user));
        }

        public Task<User> FindByNameAsync(string userName)
        {
            return userManager.GetByName(userName);
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(User user)
        {
            return Task.Run(() =>
            {
                IList<UserLoginInfo> list = new List<UserLoginInfo>
                {
                    new UserLoginInfo("Facebook", user.FacebookId)
                };

                return list;
            });
        }

        #endregion

        #region IUserLoginStore

        public Task RemoveLoginAsync(User user, UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        public Task<User> FindAsync(UserLoginInfo login)
        {
            return userManager.GetByFacebookId(login.ProviderKey);
        }

        public Task AddLoginAsync(User user, UserLoginInfo login)
        {
            return Task.Run(() => user.FacebookId = login.ProviderKey);
        }

        #endregion

        public void Dispose()
        {
            
        }
    }
}
