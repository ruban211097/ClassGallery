﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassGallery.Business.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace ClassGallery.Mvc.Identity.Managers
{
    public class IdentitySignInManager : SignInManager<User, string>
    {
        public IdentitySignInManager(UserManager<User, string> userManager,
            IAuthenticationManager authenticationManager) 
            : base(userManager, authenticationManager)
        {

        }

        public void SignOut()
        {
            AuthenticationManager.SignOut();
        }
    }
}
