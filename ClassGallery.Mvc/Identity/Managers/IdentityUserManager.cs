﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassGallery.Business.Entities;
using ClassGallery.Business.Managers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ClassGallery.Mvc.Identity.Managers
{
    public class IdentityUserManager : UserManager<User, string>
    {
        public IdentityUserManager(IUserStore<User, string> store) 
            : base(store)
        {

        }

        public override async Task<bool> IsLockedOutAsync(string userId)
        {
            Task<bool> task = Task.FromResult(false);
            return await task;
        }

        public override async Task<IdentityResult> CreateAsync(User user)
        {
            await Store.CreateAsync(user);
            return IdentityResult.Success;
        }

        public override async Task<IdentityResult> UpdateAsync(User user)
        {
            await Store.UpdateAsync(user);
            return IdentityResult.Success;
        }

        public override async Task<bool> GetTwoFactorEnabledAsync(string userId)
        {
            Task<bool> task = Task.FromResult(false);
            return await task;
        }
    }
}
