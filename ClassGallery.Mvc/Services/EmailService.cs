﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.Web.Configuration;

namespace ClassGallery.Mvc.Services
{
    public class EmailService
    {
        public async Task SendLetterAsync(string receiver, string subject, string body)
        {
            var sender = WebConfigurationManager.AppSettings["emailAddressForSendingLetters"];

            using (var smtp = new SmtpClient())
            using (var mail = new MailMessage(sender, receiver, subject, body))
            {
                mail.IsBodyHtml = true;

                await smtp.SendMailAsync(mail);
            }
        }
    }
}
