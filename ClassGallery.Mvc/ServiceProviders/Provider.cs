﻿using ClassGallery.Business.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ClassGallery.Business.Providers.ServiceProviders;
using ClassGallery.Business.Interfaces;
using System.Linq;
using System.Text;

namespace ClassGallery.Nhibernate.Providers.ServiceProviders
{
    public class Provider<T> : IProvider<T> where T : class, IEntity
    {
        protected readonly IUnitOfWork unitOfWork;

        public Provider(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        #region HelperMethods

        public string GenerateId()
        {
            return Guid.NewGuid().ToString();
        }

        public string GenerateTokenById(T entity)
        {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            return Convert.ToBase64String(time.Concat(Encoding.UTF8.GetBytes(entity.Id)).ToArray());
        }

        #endregion

        public Task<T> Get(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));

            return unitOfWork.Session.GetAsync<T>(id);
        }

        public async Task<T> SaveOrUpdate(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (string.IsNullOrEmpty(entity.Id))
            {
                entity.Id = Guid.NewGuid().ToString();
            }

            using (var unity = unitOfWork)
            {
                await unity.BeginTransaction();
                await unity.Session.SaveOrUpdateAsync(entity);
                await unity.Commit();
            }

            return entity;
        }

        public async Task<bool> IsExist(string id)
        {
            return await unitOfWork.Session.GetAsync<T>(id) != null;
        }

        public Task<IList<T>> GetList()
        {
            return unitOfWork.Session.QueryOver<T>().ListAsync();
        }

        public async Task Delete(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            
            if (string.IsNullOrEmpty(entity.Id))
                throw new ArgumentException(nameof(entity) + nameof(entity.Id));
            
            using (var unity = unitOfWork)
            {
                await unity.BeginTransaction();
                await unity.Session.DeleteAsync(entity);
                await unity.Commit();
            }
        }

        public async Task Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));
            
            await Delete(await Get(id));
        }

        public Task<T> Create(T entity)
        {
            throw new NotImplementedException();
        }

        public async Task Update(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            
            using (var unity = unitOfWork)
            {
                await unity.BeginTransaction();
                await unity.Session.UpdateAsync(entity);
                await unity.Commit();
            }
        }
    }
}
