﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Mvc.ViewModels
{
    public class ErrorViewModel
    {
        public int StatusCode { get; set; }

        public string StatusMesage { get; set; }
    }
}
