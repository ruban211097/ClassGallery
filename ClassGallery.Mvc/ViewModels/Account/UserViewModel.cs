﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Mvc.ViewModels
{
    public class UserViewModel
    {
        [ScaffoldColumn(false)]
        public string Id { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "Username")]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "Password")]
        public string Password { get; set; }
    }
}
