﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Mvc.ViewModels
{
    public class FacebookUserViewModel
    {
        public string Id { get; set; }

        [Required]
        public string AccessToken { get; set; }

        [Required]
        public string FacebookUserId { get; set; }
    }
}
