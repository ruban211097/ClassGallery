﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Mvc.ViewModels
{
    public class GroupMember
    {
        public virtual string Name { get; set; }

        public virtual string ProfileUrl { get; set; }

        public virtual string ImageUrl { get; set; }
    }
}
