﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Mvc.ViewModels
{
    public class GroupViewModel
    {
        [ScaffoldColumn(false)]
        public string Id { get; set; }
       
        [DataType(DataType.Text)]
        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "NameRequired")]
        [StringLength(255, MinimumLength = 1, ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "NameStringLength")]
        public string Name { get; set; }

        [DataType(DataType.Text)]
        [StringLength(255, MinimumLength = 1, ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "DescriptionStringLength")]
        public string Description { get; set; }

        [ScaffoldColumn(false)]
        public int RequestsNumber { get; set; }

        [ScaffoldColumn(false)]
        public string CreatorId { get; set; }

        [ScaffoldColumn(false)]
        public string InviteLink { get; set; }

        [ScaffoldColumn(false)]
        public bool IsInGroup { get; set; }
    }
}
