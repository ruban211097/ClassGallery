﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ClassGallery.Mvc.ViewModels
{
    public class UserRequestViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public string UserName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string UserGroupId { get; set; }

        [ScaffoldColumn(false)]
        public bool IsConfirmed { get; set; }
    }
}
