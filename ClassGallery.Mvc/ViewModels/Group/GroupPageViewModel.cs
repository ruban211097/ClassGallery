﻿using ClassGallery.Facebook.Entities;
using SocialNetworkService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Mvc.ViewModels
{
    public class GroupPageViewModel
    {
        public IEnumerable<GroupMember> briefUsersInfo { get; set; }

        public string InviteLink { get; set; }

        public List<IPhoto> Photos { get; set; }

    }
}
