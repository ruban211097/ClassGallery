﻿using AutoMapper;
using ClassGallery.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace ClassGallery.Mvc.ViewModels
{
    public class ViewModelsMapperProfile : AutoMapper.Profile
    {
        public ViewModelsMapperProfile()
        {
            CreateMap<Group, GroupViewModel>().ForMember(g => g.InviteLink, g => g.MapFrom(c => c.InviteToken));
            CreateMap<GroupViewModel, Group>();

            CreateMap<UserGroup, UserRequestViewModel>().ForMember(g => g.UserGroupId, g => g.MapFrom(c => c.Id));
            CreateMap<UserRequestViewModel, UserGroup>();
        }
    }
}
